Source: numpy-stl
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Bernd Zeimetz <bzed@debian.org>
Build-Depends: debhelper-compat (= 10), dh-python,
# Python development files
 python3-all-dev,
#Python setuptools
 python3-setuptools,
# Cython*
 cython3,
# Numpy*
 python3-numpy,
# PyTest
 python3-pytest, python3-pytest-runner,
Standards-Version: 4.1.1.0
Homepage: https://github.com/WoLpH/numpy-stl
Vcs-Git: https://salsa.debian.org/python-team/packages/numpy-stl.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/numpy-stl

Package: numpy-stl
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
 python3-stl
Description: stl file and 3d object handling (tools)
 Numpy-STL is a library for working with STL files (and 3D objects in
 general) in the Python language.
 Due to all operations heavily relying on Numpy this is one of the fastest
 STL editing libraries for Python available.
 .
 This package contains tools to convert between the ascii and binary
 stl file format versions, using Numpy-STL.

Package: python3-stl
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 ${python3:Depends},
 python3-python-utils,
Provides: python3-numpy-stl
Breaks: python3-numpy-stl
Replaces: python3-numpy-stl
Description: stl file and 3d object handling for the Python language
 Numpy-STL is a library for working with STL files (and 3D objects in
 general) in the Python language.
 Due to all operations heavily relying on Numpy this is one of the fastest
 STL editing libraries for Python available.
